package Leonov;

import java.io.IOException;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Orbita_10 {
    public static void main(String[] args) throws IOException  {
        PNS24_Puma objPNS24_Puma = new PNS24_Puma();

        objPNS24_Puma.menuPNS24Puma();
    }
}
