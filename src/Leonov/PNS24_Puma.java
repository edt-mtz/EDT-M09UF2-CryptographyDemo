package Leonov;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

class PNS24_Puma {

    private void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("Toca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }


    void menuPNS24Puma() throws IOException {
        String opcio;
        Scanner sc = new Scanner(System.in);
        Kristall objKristall = new Kristall();

        do {
            System.out.println("---------------- PNS-24 Puma ----------------");
            System.out.println("7. Sistema de grabació Kristall");
            System.out.println();
            System.out.println("50. Tornar al menú pare (PNS-24 Puma)");
            System.out.println();
            System.out.print("opció?: ");
            opcio = sc.nextLine();

            switch (opcio) {
                case "7":
                    objKristall.menuKristall();
                    bloquejarPantalla();
                    break;
                case "50":
                    break;
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }
        } while (!opcio.equals("50"));
    }
}