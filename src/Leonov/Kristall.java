package Leonov;

import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.*;
import java.util.Scanner;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

class Kristall {

    public Kristall() {
    }

    String folder = "encriptats";
    File dir = new File(folder);

    static SecretKey secretKey; // menu 1 y 11
    static KeyPair keyPair; // menu 11
    static byte[] textEncriptat; // menu 2
    static byte[] textDesencriptat; // menu 3
    static byte[][] textEncriptatAsim; // menu 12
    static byte[] textDesencriptatAsim; // menu 13

    private static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("Toca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }

    static final byte[] IV_PARAM = {0x00, 0x01, 0x02, 0x03,
            0x04, 0x05, 0x06, 0x07,
            0x08, 0x09, 0x0A, 0x0B,
            0x0C, 0x0D, 0x0E, 0x0F};

    /**
     * Menu 1
     */
    private SecretKey generadorDeClausSimetriques(int keySize) {
        SecretKey sKey = null;
        if ((keySize == 128) || (keySize == 192) || (keySize == 256)) {
            try {
                KeyGenerator kgen = KeyGenerator.getInstance("AES");
                kgen.init(keySize);
                sKey = kgen.generateKey();
            } catch (NoSuchAlgorithmException ex) {
                System.err.println("Generador no disponible.");
            }
        }
        return sKey;
    }

    /**
     * Menu 11
     */
    private KeyPair generadorDeClausAsimetriques(int size) {
        KeyPair keys = null;
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(size);
            keys = keyGen.genKeyPair();
        } catch (Exception ex) {
            System.err.println("Generador no disponible.");
        }
        return keys;
    }

    /**
     * Menu 2
     */
    private byte[] encriptarAES(SecretKey sKey, String data) {
        byte[] encryptedData = null;
        byte[] dataB = data.getBytes();
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, sKey);
            encryptedData = cipher.doFinal(dataB);
            String dataEnString = new BASE64Encoder().encode(encryptedData); // Mostrar lo encriptado en String

            System.out.println("Text sense encriptat: " + data);
            System.out.println("Text encriptat: " + dataEnString);
        } catch (Exception ex) {
            System.err.println("Error xifrant les dades: " + ex);
        }
        return encryptedData;
    }

    /**
     * Menu 3
     */
    private byte[] desencriptarAES(SecretKey sKey, byte[] data) {
        byte[] decryptedData = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, sKey);
            decryptedData = cipher.doFinal(data);

            String decrypterDataEnString = new String(decryptedData);
            String dataEnString = new BASE64Encoder().encode(data); // Mostrar lo encriptado en String

            System.out.println("Text a desencriptar: " + dataEnString);
            System.out.println("Text desencriptat (String): " + decrypterDataEnString);

        } catch (Exception ex) {
            System.err.println("Error xifrant les dades: " + ex);
        }
        return decryptedData;
    }

    /**
     * Menu 12
     */
    private byte[][] encriptarRSA(SecretKey clauSecretaSimetrica, KeyPair clauPublicaIPrivada, String text) {
        byte[] dadesAEncriptarEnByte;
        byte[] dadesEncriptadesEnByte;
        String dadesEncriptadesEnString;

        PublicKey clauPublica = clauPublicaIPrivada.getPublic();
        byte[] clauAESEncriptadaEnByte;
        byte[][] dadesIClauEncriptadesEnByte = new byte[2][];
        String clauEncriptadaEnString;

        System.out.println("Text a encriptar en String:" + text);
        System.out.println();

        dadesAEncriptarEnByte = text.getBytes();

        try {
            //Encriptem les dades amb AES en mode CBC.
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(IV_PARAM);
            cipher.init(Cipher.ENCRYPT_MODE, clauSecretaSimetrica, iv);
            dadesEncriptadesEnByte = cipher.doFinal(dadesAEncriptarEnByte);

            dadesEncriptadesEnString = new BASE64Encoder().encode(dadesEncriptadesEnByte);
            System.out.println("Text a encriptar en String: " + dadesEncriptadesEnString);
            System.out.println();

            //Encriptem la clau d'encriptació que s'ha fet servir amb RSA + la clau pública.
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.WRAP_MODE, clauPublica);
            clauAESEncriptadaEnByte = cipher.wrap(clauSecretaSimetrica);
            dadesIClauEncriptadesEnByte[0] = dadesEncriptadesEnByte;
            dadesIClauEncriptadesEnByte[1] = clauAESEncriptadaEnByte;

            clauEncriptadaEnString = new BASE64Encoder().encode(clauAESEncriptadaEnByte);
            System.out.println("Clau encriptada en String = " + clauEncriptadaEnString);

        } catch (Exception ex) {
            System.err.println("ERROR: R.E.menu12() " + ex);
        }

        return dadesIClauEncriptadesEnByte;
    }

    /**
     * Menu 13
     */
    private byte[] desencriptarRSA(KeyPair clauPublicaIPrivada, byte[][] dadesIClauEncriptadesEnByte) {
        PrivateKey clauPrivada;
        Key clauAESDesencriptada;
        byte[] dadesDesencriptadesEnByte;
        String dadesDesencriptadesEnString = "";

        try {
            //Desencriptem la clau d'encriptació que s'ha fet servir amb RSA + la clau privada.
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            clauPrivada = clauPublicaIPrivada.getPrivate();
            cipher.init(Cipher.UNWRAP_MODE, clauPrivada);
            clauAESDesencriptada = cipher.unwrap(dadesIClauEncriptadesEnByte[1], "AES", Cipher.SECRET_KEY);
            //Una clau simètrica és una "SECRET_KEY".

            //Desencriptem les dades amb AES en mode CBC.
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(IV_PARAM);
            cipher.init(Cipher.DECRYPT_MODE, clauAESDesencriptada, iv);
            dadesDesencriptadesEnByte = cipher.doFinal(dadesIClauEncriptadesEnByte[0]);

            dadesDesencriptadesEnString = new String(dadesDesencriptadesEnByte);
            System.out.println("Text desencriptat String: " + dadesDesencriptadesEnString);

        } catch (Exception ex) {
            System.err.println("ERROR: R.E.menu13() " + ex);
        }
        return dadesDesencriptadesEnString.getBytes();
    }

    /**
     * Menu 21 / 31
     */
    String fileAES_Encriptat = "FITXER_DADES_XIFRADES_AES128";
    String fileAES_Desencriptat = "FITXER_DADES_DESXIFRADES_AES128";
    File fileDirAES_Encriptat = new File(folder + File.separator + fileAES_Encriptat);
    File fileDirAES_Desencriptat = new File(folder + File.separator + fileAES_Desencriptat);

    /**
     * Menu 21
     */
    private void encriptarAESAFitxer(SecretKey clauSecretaSimetrica, String dades) {
        System.out.println("dadesAEncriptarEnString: " + dades);
        System.out.println();
        byte[] dadesAEncriptarEnByte = dades.getBytes();

        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            // Preparem l'encriptació.
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(IV_PARAM);
            cipher.init(Cipher.ENCRYPT_MODE, clauSecretaSimetrica, iv);
            // Encriptem les dades amb AES en mode CBC directament a un fitxer.
            CipherOutputStream cos = new CipherOutputStream(new FileOutputStream(fileDirAES_Encriptat), cipher);
            cos.write(dadesAEncriptarEnByte);
            cos.close();
        } catch (Exception ex) {
            System.err.println("ERROR: menu21() " + ex);
        }
    }


    /**
     * Menu 31
     */
    private void desencriptarAESDesdeFitxer(SecretKey clauSecretaSimetrica) {
        try {
            // Desencriptem les dades amb AES en mode CBC.
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(IV_PARAM);
            cipher.init(Cipher.DECRYPT_MODE, clauSecretaSimetrica, iv);

            CipherInputStream cis = new CipherInputStream(new FileInputStream(fileDirAES_Encriptat), cipher);
            BufferedReader br = new BufferedReader(new InputStreamReader(cis));

            // Preparem el fitxer per guardar les dades desxifrades.
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDirAES_Desencriptat, true)));

            System.out.print("dadesDesencriptadesEnString: ");

            String temp;
            while ((temp = br.readLine()) != null) {
                bw.write(temp + System.lineSeparator());
                System.out.println(temp);
            }
            System.out.println();
            br.close();
            bw.close();
        } catch (Exception ex) {
            System.err.println("ERROR: menu31() " + ex);
        }
    }

    /**
     * Menu 22 / 32
     */

    String fileRSA_Encriptat_1 = "FITXER_DADES_XIFRADES_RSA1024";
    String fileRSA_Encriptat_key = "FITXER_DADES_XIFRADES_KEY_RSA1024";
    String fileRSA_Desencriptat = "FITXER_DADES_DESXIFRADES_RSA1024";
    File fileDirRSA_Encriptat_1 = new File(folder + File.separator + fileRSA_Encriptat_1);
    File fileDirRSA_Encriptat_key = new File(folder + File.separator + fileRSA_Encriptat_key);
    File fileDirRSA_Desencriptat = new File(folder + File.separator + fileRSA_Desencriptat);

    /**
     * Menu 22
     */
    private void encriptarRSAAFitxer(SecretKey clauSecretaSimetrica, PublicKey publicKey, String dades) {

        System.out.println("dadesAEncriptarEnString: " + dades);
        System.out.println();
        byte[] dadesAEncriptarEnByte = dades.getBytes();

        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            // Preparem l'encriptació del text amb la clau simetrica.
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(IV_PARAM);
            cipher.init(Cipher.ENCRYPT_MODE, clauSecretaSimetrica, iv);
            // Encriptem les dades amb AES en mode CBC directament a un fitxer.
            CipherOutputStream cos = new CipherOutputStream(new FileOutputStream(fileDirRSA_Encriptat_1), cipher);
            cos.write(dadesAEncriptarEnByte);
            cos.close();

            //Encriptem la clau d'encriptació que s'ha fet servir amb RSA + la clau pública.
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.WRAP_MODE, publicKey);
            byte[] clauAESEncriptadaEnByte = cipher.wrap(clauSecretaSimetrica);
            // Encriptem les dades amb AES en mode CBC directament a un fitxer.
            FileOutputStream fos = new FileOutputStream(fileDirRSA_Encriptat_key);
            fos.write(clauAESEncriptadaEnByte);
            fos.close();
        } catch (Exception ex) {
            System.err.println("ERROR: menu21() " + ex);
        }
    }

    /**
     * Menu 32
     */
    private void desencriptarRSADesdeFitxer(PrivateKey privateKey) {
        try {
            //Desencriptem la clau d'encriptació que s'ha fet servir amb RSA + la clau privada.
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.UNWRAP_MODE, privateKey);

            byte[] bytesClau = new byte[(int)fileDirRSA_Encriptat_key.length()];
            FileInputStream fis = new FileInputStream(fileDirRSA_Encriptat_key);
            fis.read(bytesClau);

            Key clauAESDesencriptada = cipher.unwrap(bytesClau, "AES", Cipher.SECRET_KEY);

            System.out.println();

            System.out.println("clauDesencriptadaEnString: " + new BASE64Encoder().encode(bytesClau));

            // Desencriptem les dades amb AES en mode CBC.
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(IV_PARAM);
            cipher.init(Cipher.DECRYPT_MODE, clauAESDesencriptada, iv);

            CipherInputStream cis = new CipherInputStream(new FileInputStream(fileDirRSA_Encriptat_1), cipher);
            BufferedReader br2 = new BufferedReader(new InputStreamReader(cis));

            // Preparem el fitxer per guardar les dades desxifrades.
            BufferedWriter bw2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDirRSA_Desencriptat, true)));

            System.out.print("dadesDesencriptadesEnString: ");

            String temp2;
            while ((temp2 = br2.readLine()) != null) {
                bw2.write(temp2 + System.lineSeparator());
                System.out.println(temp2);
            }
            System.out.println();
            br2.close();
            bw2.close();


        } catch (Exception ex) {
            System.err.println("ERROR: R.E.menu32() " + ex);
        }
    }

    void menuKristall() throws IOException {
        String opcio;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("---------------- Evkalipt ----------------");
            System.out.println();
            System.out.println("   ENCRIPTACIO SIMÈTRICA (AES 128 bits)");
            System.out.println("1. Generar clau simètrica");
            System.out.println("2. Encriptar cadena de text amb AES amb clau embolcallada");
            System.out.println("3. Desencriptar cadena de text amb AES amb clau embolcallada");
            System.out.println();
            System.out.println("   ENCRIPTACIO ASIMÈTRICA (RSA amb clau embolcallada)");
            System.out.println("11. Generar clai simètrica i público-privades");
            System.out.println("12. Encriptar cadena de text amb RSA amb clau embolcallada");
            System.out.println("13. Desencriptar cadena de text amb RSA amb clau embolcallada");
            System.out.println();
            System.out.println("   ENCRIPTACIO DE DADES (cap a fitxer)");
            System.out.println("21. Encriptar amb AES 128 bits els tripulants");
            System.out.println("22. Encriptar amb RSA amb clau embolcallada els tripulants");
            System.out.println();
            System.out.println("   DESEENCRIPTACIO DE DADES (desde fitxer)");
            System.out.println("31. Desencriptar amb AES 128 bits els tripulants");
            System.out.println("32. Desencriptar amb RSA amb clau embolcallada els tripulants");
            System.out.println();
            System.out.println("50. Tornar al menú pare (PNS-24 Puma)");
            System.out.println();
            System.out.print("opció?: ");
            opcio = sc.nextLine();
            String text;

            switch (opcio) {
                case "1":
                    secretKey = generadorDeClausSimetriques(128);
                    System.out.println("Clau simetrica generada: " + secretKey);
                    bloquejarPantalla();
                    break;
                case "2":
                    if (secretKey != null) {
                        System.out.print("Text a encriptar: ");
                        text = sc.nextLine();
                        textEncriptat = encriptarAES(secretKey, text);
                    }
                    bloquejarPantalla();
                    break;
                case "3":
                    if (secretKey != null && textEncriptat != null) {
                        textDesencriptat = desencriptarAES(secretKey, textEncriptat);
                    } else {
                        System.out.println("Has de executar abans las opciones de menu 1 y 2");
                    }
                    bloquejarPantalla();
                    break;
                case "11":
                    secretKey = generadorDeClausSimetriques(128);
                    System.out.println("Clau simetrica generada: " + secretKey);
                    keyPair = generadorDeClausAsimetriques(1024);
                    System.out.println("Clau asimetrica generada:");
                    System.out.println("\tPublica: " + keyPair.getPublic());
                    System.out.println();
                    System.out.println("\tPrivada: " + keyPair.getPrivate());
                    bloquejarPantalla();
                    break;
                case "12":
                    if (secretKey != null && keyPair != null) {
                        System.out.print("Text a encriptar: ");
                        text = sc.nextLine();
                        textEncriptatAsim = encriptarRSA(secretKey, keyPair, text);
                    } else {
                        System.out.println("Has de executar abans la opció de menu 11");
                    }
                    bloquejarPantalla();
                    break;
                case "13":
                    textDesencriptatAsim = desencriptarRSA(keyPair, textEncriptatAsim);
                    bloquejarPantalla();
                    break;
                case "21":
                    if (secretKey != null) {
                        System.out.print("Text a encriptar: ");
                        text = sc.nextLine();
                        encriptarAESAFitxer(secretKey, text);
                    } else {
                        System.out.println("Has de executar abans la opció de menu 1 ó 11");
                    }
                    bloquejarPantalla();
                    break;
                case "22":
                    if (secretKey != null && keyPair != null) {
                        System.out.print("Text a encriptar: ");
                        text = sc.nextLine();
                        encriptarRSAAFitxer(secretKey, keyPair.getPublic(), text);
                        bloquejarPantalla();
                    } else {
                        System.out.println("Has de executar abans la opció de menu 11");
                    }
                    break;
                case "31":
                    if (secretKey != null && fileDirAES_Encriptat.exists()) {
                        desencriptarAESDesdeFitxer(secretKey);
                    } else {
                        System.out.println("Has de executar abans la opció de menu 1 ó 11 y despres la opcio 21");
                    }
                    bloquejarPantalla();
                    break;
                case "32":
                    if (secretKey != null) {
                        desencriptarRSADesdeFitxer(keyPair.getPrivate());
                    } else {
                        System.out.println("Has de executar abans la opció de menu 11 y despres la opcio 32");
                    }
                    bloquejarPantalla();
                    break;
                case "50":
                    break;
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }
        } while (!opcio.equals("50"));
    }

}
